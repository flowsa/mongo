<?php
/**
 * Mongo plugin for Craft CMS 3.x
 *
 * Imports Mongo DB
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

/**
 * Mongo en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('mongo', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow Communications
 * @package   Mongo
 * @since     1.0.0
 */
return [
    'Mongo plugin loaded' => 'Mongo plugin loaded',
];
