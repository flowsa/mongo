/**
 * Mongo plugin for Craft CMS
 *
 * MongoWidget Widget JS
 *
 * @author    Flow Communications
 * @copyright Copyright (c) 2019 Flow Communications
 * @link      www.flowsa.com
 * @package   Mongo
 * @since     1.0.0
 */
