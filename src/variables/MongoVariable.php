<?php
/**
 * Mongo plugin for Craft CMS 3.x
 *
 * Imports Mongo DB
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\mongo\variables;

use flowsa\mongo\Mongo;

use Craft;

/**
 * Mongo Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.mongo }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Flow Communications
 * @package   Mongo
 * @since     1.0.0
 */
class MongoVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.mongo.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.mongo.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
