<?php
/**
 * Mongo plugin for Craft CMS 3.x
 *
 * Imports Mongo DB
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\mongo\utilities;

use flowsa\mongo\Mongo;
use flowsa\mongo\assetbundles\mongoutilityutility\MongoUtilityUtilityAsset;

use Craft;
use craft\base\Utility;

/**
 * Mongo Utility
 *
 * Utility is the base class for classes representing Control Panel utilities.
 *
 * https://craftcms.com/docs/plugins/utilities
 *
 * @author    Flow Communications
 * @package   Mongo
 * @since     1.0.0
 */
class MongoUtility extends Utility
{
    // Static
    // =========================================================================

    /**
     * Returns the display name of this utility.
     *
     * @return string The display name of this utility.
     */
    public static function displayName(): string
    {
        return Craft::t('mongo', 'MongoUtility');
    }

    /**
     * Returns the utility’s unique identifier.
     *
     * The ID should be in `kebab-case`, as it will be visible in the URL (`admin/utilities/the-handle`).
     *
     * @return string
     */
    public static function id(): string
    {
        return 'mongo-mongo-utility';
    }

    /**
     * Returns the path to the utility's SVG icon.
     *
     * @return string|null The path to the utility SVG icon
     */
    public static function iconPath()
    {
        return Craft::getAlias("@flowsa/mongo/assetbundles/mongoutilityutility/dist/img/MongoUtility-icon.svg");
    }

    /**
     * Returns the number that should be shown in the utility’s nav item badge.
     *
     * If `0` is returned, no badge will be shown
     *
     * @return int
     */
    public static function badgeCount(): int
    {
        return 0;
    }

    /**
     * Returns the utility's content HTML.
     *
     * @return string
     */
    public static function contentHtml(): string
    {
        Craft::$app->getView()->registerAssetBundle(MongoUtilityUtilityAsset::class);

        $someVar = 'Have a nice day!';
        return Craft::$app->getView()->renderTemplate(
            'mongo/_components/utilities/MongoUtility_content',
            [
                'someVar' => $someVar
            ]
        );
    }
}
