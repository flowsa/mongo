<?php


/**
 * Mongo plugin for Craft CMS 3.x
 *
 * Imports Mongo DB
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communications
 */

namespace flowsa\mongo\console\controllers;

use flowsa\mongo\Mongo;


use MongoDB\Client as Mongo2;

use yii\console\Controller;
use yii\helpers\Console;

use Craft;
use craft\elements\MatrixBlock;
use craft\elements\Asset;
use craft\base\Element;
use craft\elements\Entry;
use craft\guestentries\events\SaveEvent;
use craft\guestentries\models\SectionSettings;
use craft\guestentries\models\Settings;
use craft\guestentries\Plugin;
use craft\helpers\DateTimeHelper;
use craft\helpers\Db;
use craft\models\Section;
use craft\web\Request;
use craft\helpers\Assets;

/**
 * Default Command
 *
 * The first line of this class docblock is displayed as the description
 * of the Console Command in ./craft help
 *
 * Craft can be invoked via commandline console by using the `./craft` command
 * from the project root.
 *
 * Console Commands are just controllers that are invoked to handle console
 * actions. The segment routing is plugin-name/controller-name/action-name
 *
 * The actionIndex() method is what is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft mongo/default
 *
 * Actions must be in 'kebab-case' so actionDoSomething() maps to 'do-something',
 * and would be invoked via:
 *
 * ./craft mongo/default/do-something
 *
 * @author    Flow Communications
 * @package   Mongo
 * @since     1.0.0
 */

define("SOURCE_HOSTNAME", "http://www.wesgro.co.za");

class DefaultController extends Controller
{


  public $regex;


    // Public Methods
    // =========================================================================

    /**
     * Handle mongo/default console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionIndex()
    {


    }




 // "TradeArticle", 
 //                "TradeEvent", 
 //                "InvestArticle", 
 //                "InvestEvent", 
 //                "FilmArticle", 
 //                "FilmEvent", 
 //                "WesgroArticle", 
 //                "SpecialProjectArticle", 
 //                "SpecialProjectEvent", 
 //                "WesgroEvent", 
 //                "TourismArticle", 
 //                "Research", 
 //                "TourismEvent", 
 //                "News"


  public function actionImport($documentType, $regex = false) {

    $file = $documentType.".json";
    $data = json_decode(file_get_contents($file));

    if($documentType == 'blog'){
      foreach($data as $blog){
        $entry = $this->import_news($blog);
      }
    }
    
    if($documentType == 'authors'){
      foreach($data as $author){
        $entry = $this->import_authors($author);
      }
    }

    if($documentType == 'teachers'){
      foreach($data as $teachers){
        $entry = $this->import_teachers($teachers);
      }
    }

    if($documentType == 'audio'){
      foreach($data as $audio){
        $entry = $this->import_audio($audio);
      }
    }

  }

  public function actionImportDocs($documentType, $collection, $searchPhrase)
  {

    $cursor = $collection->find($searchPhrase);
    $totalDocs = $collection->count($searchPhrase);

    $entries = [];

    Console::startProgress(0, $totalDocs);

    $count = 0; 

    foreach ($cursor as $docKey => $doc) {

      $this->greenMessage("[$docKey] $doc->path " . $doc->data->name->en);

      $doc = json_decode(json_encode($doc));

      $importMethod = "import_" . $documentType;

        // spawn customised import method
      $entry = $this->{$importMethod}($doc);

      // var_dump($entry->id);

      $this->italicMessage("Completed: [$docKey] 
      Title: $entry->title
      Site: $entry->site
      Section: $entry->section
      Updated/Created on: " . $entry->dateUpdated->format('Y-m-d H:i:s'));

      $count = $count + 1;

      Console::updateProgress($count, $totalDocs);

    }

    Console::endProgress();

    exit;


  }

  public function import_news($doc) {

    // All Simple Entries (e.g. Categories) are just a title
    $item["displayTitle"] = $doc->title;
    // $imageArray = [];
    // if(isset($doc->blog_imagery)){
    //   foreach($doc->blog_imagery as $images){

    //     if(isset($doc->blog_imagery_feature) && $images->fullpath == $doc->blog_imagery_feature){
    //       $item["featureImageLandscape"] = [$this->uploadAsset($images)];
    //     }
    //   }
    //   $imageArray = $doc->blog_imagery;

    // }
    echo $doc->title;
    
    $item["body"] = $this->generatedContentBody($doc->body);


    // $item["relatedAuthors"] = $authors;

    // $categories = [];
    // if(isset($doc->blog)){
    //   foreach($doc->blog as $cats){
    //     $categories[] = $this->createSimpleEntry(3,"newsCategory", $cats->name);
        
    //   }
    //   $item["newsCategories"] = $categories;

    // }
    // $item["byline"] = $doc->byline;
    // $item["soundcloudOld"] = $doc->blog_soundcloud;
  
    $item["summary"] = $doc->summary;




    $entryFormat = [
        "siteId" => 1,
        'sectionId' => 21,
        'authorId' => 1,
        'postDate'=> new \DateTime($doc->postDate),
        'typeId' => 25,
        "title" => $doc->title,
        'enabled' => 1,
          'enabledForSite' => true // other sites should default false
        ];

    $entry = $this->createEntry($item, $entryFormat);
    return $entry;
    
  }



  public function import_authors($doc) {

    // All Simple Entries (e.g. Categories) are just a title
      $item["displayTitle"] = $doc->name;

      if(isset($doc->photo)){
        $item["featureImageLandscape"] = [$this->uploadAsset($doc->photo)];
      }
      echo $doc->name;
      // dump($doc->body[0]);
      // exit;
      $item["body"] = $this->generatedContentBody($doc->body[0],null);




      $entryFormat = [
          "siteId" => 3,
          "importId" => $doc->import_id,
          'sectionId' => 14,
          'authorId' => 1,
          'postDate'=> new \DateTime($doc->entry_date),
          'typeId' => 17,
          "title" => $doc->title,
          'enabled' => 1,
            'enabledForSite' => true // other sites should default false
          ];

      $entry = $this->createEntry($item, $entryFormat);
      return $entry;
    
  }

  public function import_teachers($doc) {

    // All Simple Entries (e.g. Categories) are just a title
      $item["displayTitle"] = $doc->display_title;
      $photo = [];
      $photo['fullpath'] = $doc->teacher_image;
      $photo['filename'] = $doc->filename;
      $photo = json_decode(json_encode($photo));

      $item["featureImageLandscape"] = [$this->uploadAsset($photo)];
      echo $doc->display_title;
      // dump($doc->body[0]);
      // exit;
      $item["summary"] = $doc->teacher_summary;


      $entryFormat = [
          "siteId" => 4,
          "importId" => $doc->import_id,
          'sectionId' => 50,
          'authorId' => 1,
          'postDate'=> new \DateTime($doc->entry_date),
          'typeId' => 60,
          "title" => $doc->title,
          'enabled' => 1,
            'enabledForSite' => true // other sites should default false
          ];

      $entry = $this->createEntry($item, $entryFormat);
      return $entry;
    
  }

  public function import_audio($doc) {

    // All Simple Entries (e.g. Categories) are just a title
      $item["displayTitle"] = $doc->display_title;

      $photo = [];
      $photo['fullpath'] = $doc->audio_image_banner;
      $photo['filename'] = $doc->filename;
      $photo = json_decode(json_encode($photo));
      if($photo->fullpath &&  $photo->filename){
        $item["featureImageLandscape"] = [$this->uploadAsset($photo)];
      }
      // dump($doc->body[0]);
      // exit;
      $arr["MatrixBlock"] = [];
      $count = 0;

      if($doc->audio_long_description){
        $arr["MatrixBlock"][$count]['text']['text'] = $doc->audio_long_description;
        $count++;
      }

      if($doc->audio_files){
        foreach($doc->audio_files as $audio){
          $arr["MatrixBlock"][$count]['audio'] = $audio;
          $count++;
        }
      }

      $arr = json_decode(json_encode($arr));
      $item["body"] = $this->generatedContentBody($arr,null);
      
      $item["summary"] = $doc->audio_summary;
      $item["byline"] = $doc->byline;
      $categories = [];
      if(isset($doc->audio)){
        foreach($doc->audio as $cats){
          $categories[] = $this->createSimpleEntry(3,"podcastCategory", $cats->name);
          
        }
        $item["podcastCategories"] = $categories;
  
      }

      

      $entryFormat = [
          "siteId" => 3,
          "importId" => $doc->import_id,
          'sectionId' => 13,
          'authorId' => 1,
          'postDate'=> new \DateTime($doc->entry_date),
          'typeId' => 16,
          "title" => $doc->title,
          'enabled' => 1,
            'enabledForSite' => true // other sites should default false
          ];

      $entry = $this->createEntry($item, $entryFormat);
      return $entry;
    
  }

  public function getExistingEntryById($id,$sId) {
    dump($sId);
    $entry = Entry::find()
    ->search("importId:$id")
    ->anyStatus()
    ->siteId($sId)
    ->one();
    return $entry;

  }

  public function getExistingEntryByTitle($title,$sId){
    dump($sId);
    $entry = Entry::find()
    ->search("title:$title")
    ->anyStatus()
    ->siteId($sId)
    ->one();
    return $entry;
  }

  public function createEntry($item, $entryMeta) {
    // dump($entryMeta['title']);
    // exit;
    $entry = $this->getExistingEntryByTitle($entryMeta['title'], $entryMeta['siteId']);

    if($entryMeta['sectionId'] == 14){
      $entry = $this->getExistingEntryByTitle($entryMeta['importId'], $entryMeta['siteId']);

    }

    if($entryMeta['sectionId'] == 50){
      $entry = $this->getExistingEntryByTitle($entryMeta['importId'], $entryMeta['siteId']);

    }

        if(!$entry) {

          $this->redMessage("No Entry, creating new...");

         $entry = new Entry($entryMeta);

        } else {
        
        $this->greenMessage("Entry exists, updating existing");

        }

        // What if section ID needs to change? need to include entry meta here

        // Set the custom field values (including Matrix)
        $entry->setFieldValues($item);

        $result = Craft::$app->getElements()->saveElement($entry, true, true);

        if(!$result) {

          $this->redMessage("Entry failed to create");

          // Craft::error('Couldnâ€™t save the entry "'.$entry->title.'"', __METHOD__);

        } else {

          return $entry;        

        }
  }
   
  public function generateTitle($title) {
    if ($title!="") {
      return $title;
    } else {
      return "No title specified";
    }
  }



  public function _downloadImage($url) {

    $tempath = urlencode($url);

    $tempFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $tempath;

    // $fileContents = file_get_contents($url);

    if (!$data = @file_get_contents($url)) {
      $error = error_get_last();
      echo "HTTP request failed. Error was: " . $error['message'];
      return false;
    } else {
      file_put_contents($tempFilePath, $data);
      return $tempFilePath;

    }

  }


  public function _uploadNewAsset($uploadedFilePath,$filename, $folderId, $volumeId = 1) {

   //var_dump($fileName);


    $asset = $this->createAsset($uploadedFilePath,$filename,$folderId);



    return $asset;


  }


  // Borrowed from feedme

  public function createAsset($tempFilePath, $filename, $folderId)
  {
    $assets = Craft::$app->getAssets();

    $folder = $assets->findFolder(['id' => $folderId]);

        // Create the new asset (even if we're setting it to replace)
    $asset = new Asset();
    $asset->tempFilePath = $tempFilePath;
    $asset->filename = $filename;
    $asset->newFolderId = $folder->id;
    $asset->volumeId = $folder->volumeId;
    $asset->avoidFilenameConflicts = true;
    $asset->setScenario(Asset::SCENARIO_CREATE);

    $result = Craft::$app->getElements()->saveElement($asset);

    if ($result) {
            // Annoyingly, you have to create the asset field, then move it to the temp directly, then replace the conflicting
            // asset, so there's a bit more work here than I would've thought...

            // THIS RELIES ON THE ASSETS HAVING BEEN INDEXED. CONFLICTING FILENAME IS WHAT COMES BACK FROM THE FILE SYSTEM BUT THEN THE ASSET SEARCH USES THE INDEX. THIS WAS CONFUSING.

      if ($asset->conflictingFilename !== null) {

        $this->normalMessage("Conflicting file exists - $asset->conflictingFilename . replacing ");

        $conflictingAsset = Asset::findOne(['folderId' => $folder->id, 'filename' => $asset->conflictingFilename]);

        $tempPath = $asset->getCopyOfFile();
        if(isset($conflictingAsset->filename))
        {
          $assets->replaceAssetFile($conflictingAsset, $tempPath, $conflictingAsset->filename);

        } else{

          $assets->replaceAssetFile($conflictingAsset, $tempPath,bin2hex(random_bytes(16)));

        }
        Craft::$app->getElements()->deleteElement($asset);

        return $conflictingAsset;
      } else {
        return $asset;
      }
    }

    return false;
  }

  function getRows($sql) {
      global $pdo;
      $query = $pdo->query($sql);
      $result = $query->fetchAll(PDO::FETCH_OBJ);
      return $result;
  }

  public function getCollection($channel_id) {


    $channel_rows = (new \craft\db\Query())->select(['*'])->from('exp_channels')->all();
    $channel_field_rows = (new \craft\db\Query())->select(['*'])->from('exp_channel_fields')->all();
    $matrix_col_rows = (new \craft\db\Query())->select(['*'])->from('exp_matrix_cols')->all();
    $category_group_rows = (new \craft\db\Query())->select(['*'])->from('exp_category_groups')->all();
    $category_rows = (new \craft\db\Query())->select(['*'])->from('exp_categories')->all();
    $site_rows = (new \craft\db\Query())->select(['*'])->from('exp_sites')->all();
    
    $channel_rows = json_decode(json_encode($channel_rows));
    $channel_field_rows = json_decode(json_encode($channel_field_rows));
    $matrix_col_rows = json_decode(json_encode($matrix_col_rows));
    $category_group_rows = json_decode(json_encode($category_group_rows));
    $category_rows = json_decode(json_encode($category_rows));
    $site_rows  = json_decode(json_encode($site_rows));
    
    $channel_fields = [];
    $channel_fields_by_group = [];
    $channels = [];
    $channelsByName = [];
    $category_groups = [];
    $matrix_cols_by_field = [];
    $sites = [];
    
    $rating = [
        'not applicable' => '',
        '--' => '',
        '*' => 1,
        '**' => 2,
        '***' => 3,
        '****' => 4,
        '*****' => 5,
    ];
    


    $sites[$site_rows[0]->site_id] = $site_rows[0];
    foreach ($channel_field_rows as $channel_field_row) {
        $channel_fields[$channel_field_row->field_id] = $channel_field_row;
        $channel_fields_by_group[$channel_field_row->group_id][$channel_field_row->field_id] = $channel_field_row;
    }
    
    foreach ($channel_rows as $channel_row) {
        $channels[$channel_row->channel_id] = $channel_row;
    }
    
    foreach ($matrix_col_rows as $matrix_col_row) {
        $matrix_cols_by_field[$matrix_col_row->field_id][$matrix_col_row->col_id] = $matrix_col_row;
    }
    
    foreach ($category_group_rows as $category_group) {
        $category_groups[$category_group->group_id] = $category_group;
        $category_groups[$category_group->group_id]->categories = [];
    
        foreach ($category_rows as $category_row) {
            if ($category_row->group_id == $category_group->group_id) {
                $category_groups[$category_group->group_id]->categories[] = $category_row;
            }
        }
    }
    
    
    $excludeCompleted = false;
    
    $incompleteFiles = [];
    
    $export_channels = [];
  
    
    $export_channels = empty($channel) ? array_keys($channels) : $channel;

    $finalFinalOutput = [];
  
    $rows = [];

    $channel = $channels[$channel_id];


    $site = $sites[$channel->site_id];

    $channel_name = $channel->channel_name;
    $channel_fields = $channel_fields_by_group[$channel->field_group];
  

    
    // $entries = \Craft::$app->db->createCommand()->setRawSql("SELECT *
    // FROM exp_channel_titles t
    // LEFT JOIN exp_channel_data d
    // ON d.entry_id = t.entry_id
    // WHERE (t.status = 'open'
    // OR t.status = 'featured')
    // AND t.channel_id = {$channel_id}
    // ORDER BY t.entry_id DESC
    // LIMIT 1000")->execute();
    // (new \craft\db\Query())->rawSql=;

    $entries = (new \craft\db\Query())
    ->select(['*'])
    ->from('exp_channel_titles')
    ->leftJoin(['d'=>'exp_channel_data'],'exp_channel_titles.entry_id = d.entry_id')
    ->where(['exp_channel_titles.status' => 'featured'])
    ->orWhere(['exp_channel_titles.status' => 'open'])
    ->andWhere(['exp_channel_titles.channel_id'=>$channel_id])
    ->orderBy('exp_channel_titles.entry_id DESC')
    ->limit(1000)
    ->all();

    // $total = count($entries);
    // echo "\n{$site->site_name}: {$channel_name}, ({$total})\n";
    $entries = json_decode(json_encode($entries));

    return $entries;

  }

  public function greenMessage($message) {
    $this->stdout($message . "\n", Console::FG_GREEN);
  }


  public function redMessage($message) {
    $this->stdout($message . "\n", Console::FG_RED);
  }

  public function italicMessage($message) {
    $this->stdout($message . "\n", Console::ITALIC);
  }

  public function normalMessage($message) {
    $this->stdout($message . "\n", Console::ITALIC);
  }

  public function createSimpleEntry($siteId, $sectionHandle, $title) {

    $section = Craft::$app->getSections()->getSectionByHandle($sectionHandle);

        // get first entry type
    $entryType = $section->getEntryTypes()[0]->id;

    $entry = Entry::find()
    ->section($section->handle)
    ->siteId($siteId)
    ->title($title)
    ->one();
    // dump($entry);
    // exit;
    if(!$entry) {

      $entry = new Entry([
        "siteId" => $siteId,
        'sectionId' => $section->id,
        'authorId' => 1,
              // 'postDate'=> new \DateTime(strt),
        'typeId' => $entryType,
        "title" => $title,
        'enabled' => true,
        'enabledForSite' => true
              // 'newParentId' => $request->getBodyParam('parentId'),
      ]);

      $result = Craft::$app->getElements()->saveElement($entry);

      $this->normalMessage("Related entry does not exist, creating: $entry->title  $entry->id");

    } else {

     $this->normalMessage("Simple Entry already exists: $entry->title  $entry->id");

   }

   return $entry->id;

 }

 public function actionDeleteImportedItems($section = false)
  {

    $entries = Entry::find()
      ->site('*')
      ->search("importId:*")
      ->anyStatus();

    if ($section) {
      $entries->section($section);
    }

    foreach($entries as $entry) {

      // $entry->delete();

      $result = Craft::$app->getElements()->deleteElement($entry);

      if($result) {
        $this->redMessage("Entry Deleted:" . $entry->id);
      }
      // echo "deleted";

    }

  }

  public function generatedContentBody($doc) {

    $contentIndex = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

    $contentMatrix = [];

    $contentMatrix['new0'] = [
      "modified" => 1,
      "type" => "grid",
      "enabled" => 1,
      "collapsed" => 0,
      "level" => 0
    ];

    $contentMatrix['new1'] = [
      "modified" => 1,
      "type" => "row",
      "enabled" => 1,
      "collapsed" => 0,
      "level" => 1
    ];

    $contentMatrix['new2'] = [
      "modified" => 1,
      "type" => "column",
      "enabled" => 1,
      "collapsed" => 0,
      "level" => 3
    ];

    $key = 2;
    foreach($doc as $block){

      $key = $key + 1;
      $matrixKey = "new" . $key;
      if(isset($block->text)){

        $contentMatrix[$matrixKey] = [
          "modified" => 1,
          "type" => "text",
          "enabled" => 1,
          "collapsed" => 0,
          "level" => 4,
          'fields' => [
            "richText" => $block->text,
            "align" => "notSet",
            "size" => "default"
          ]
          ];
      }
      
      if(isset($block->hr)){
        
         $contentMatrix[$matrixKey]= [
          "modified" => 1,
          "type" => "horizontalRule",
          "enabled" => 1,
          "collapsed" => 0,
          "level" => 4,
          ];
      }
    }

  //   foreach ($contentIndex as $key => $letter) {

  //      // Type

  //    $typeKey = $letter . "_type";
  //    $contentKey = $letter . "_content";

  //    $key = $key + 1;

  //    $matrixKey = "new" . $key;

  //    switch($doc->data->{$typeKey}) {

  //      case "content":

  //      $content = $doc->data->{$contentKey};
  //      $contentMatrix[$matrixKey] = $this->contentBlock($content);

  //      break;

  //      case "image":

  //      $image = $doc->data->{$contentKey};

  //      $contentMatrix[$matrixKey] = $this->imageBlock($image);
  //      break;

  //      case "iframe":

  //        // don't know what to do with iframe yet

  //      $content = $doc->data->{$contentKey};

  //      $contentMatrix[$matrixKey] = "<iframe src='$content'></iframe>";
  //      break;


  //      case "default":
  //        // do nothing
  //      break;

  //    }

  //  }

   return $contentMatrix;


 }






 public function contentBlock($content) {

   return  [
     "modified" => 1,
     "type" => "text",
     "enabled" => 1,
     "collapsed" => 0,
     "level" => 1,
     'fields' => [
       "richText" => $content,
       "align" => "notSet",
       "size" => "default"
     ]
   ];

 }



// public function generatedContentBody($doc) {

//   $contentMatrix = [];

//    foreach($doc as $key => $block){

//     $key = $key + 1;

//     $matrixKey = "new" . $key;
//       if(isset($block->text)){
//           $contentMatrix[$matrixKey]['type'] = 'copy';
//           $contentMatrix[$matrixKey]['enabled'] = 1;
//           $contentMatrix[$matrixKey]['fields']['text'] = $block->text;
//           $contentMatrix[$matrixKey]['fields']['size'] = 'normal';
//           $contentMatrix[$matrixKey]['fields']['alignment'] = 'left';        
//       }

//       if(isset($block->hr)){
        
//         $contentMatrix[$matrixKey]['type'] = 'horizontalRule';
//         $contentMatrix[$matrixKey]['enabled'] = 1;
//       }

//       if(isset($block->image)){
//         if(isset($block->image->image)){
//             if(isset($block->image->image->fullpath)){
//               $contentMatrix[$matrixKey]['type'] = 'image';
//               $contentMatrix[$matrixKey]['enabled'] = 1;
//               $contentMatrix[$matrixKey]['fields']['file'] = null;
//               $contentMatrix[$matrixKey]['fields']['file'][] = $this->imageBlock($block->image->image);
//               $contentMatrix[$matrixKey]['fields']['text'] = null;
//               $contentMatrix[$matrixKey]['fields']['ratio'] = 0;
//               $contentMatrix[$matrixKey]['fields']['alignment'] = 'full';
//               $contentMatrix[$matrixKey]['fields']['clear'] = 'none';
//             } else{
//               continue;
//             }

//         } else{
//           if(isset($block->image->fullpath)){
//             $contentMatrix[$matrixKey]['type'] = 'image';
//             $contentMatrix[$matrixKey]['enabled'] = 1;
//             $contentMatrix[$matrixKey]['fields']['file'] = null;
//             $contentMatrix[$matrixKey]['fields']['file'][] = $this->imageBlock($block->image);
//             $contentMatrix[$matrixKey]['fields']['text'] = null;
//             $contentMatrix[$matrixKey]['fields']['ratio'] = 0;
//             $contentMatrix[$matrixKey]['fields']['alignment'] = 'full';
//             $contentMatrix[$matrixKey]['fields']['clear'] = 'none';
//           }

//         }
//       }

//       if(isset($block->audio)){
//         $contentMatrix[$matrixKey]['type'] = 'audio';
//         $contentMatrix[$matrixKey]['enabled'] = 1;
//         $contentMatrix[$matrixKey]['fields']['audioUrl'] = $block->audio->fullpath;
//         $contentMatrix[$matrixKey]['fields']['extension'] = $block->audio->extension;

//       }

//       // if(isset($block->video)){
//       //   $contentMatrix[$matrixKey] = $this->imageBlock($block->image->fullpath);
//       // }

//       // switch($doc->data->{$typeKey}) {

//       //   case "content":
  
//       //   $content = $doc->data->{$contentKey};
//       //   $contentMatrix[$matrixKey] = $this->contentBlock($content);
  
//       //   break;
  
//       //   case "image":
  
//       //   $image = $doc->data->{$contentKey};
  
//       //   $contentMatrix[$matrixKey] = $this->imageBlock($image);
//       //   break;
  
//       //   case "iframe":
  
//       //     // don't know what to do with iframe yet
  
//       //   $content = $doc->data->{$contentKey};
  
//       //   $contentMatrix[$matrixKey] = "<iframe src='$content'></iframe>";
//       //   break;
  
  
//       //   case "default":
//       //     // do nothing
//       //   break;
  
//       // }
//    }

//  return $contentMatrix;


// }



public function imageBlock($image) {

 $imageId = $this->uploadAsset($image);

 return $imageId;

}


public function downloadBlock($file) {

 $fileId = $this->uploadAsset($file);

 return [
   "modified" => 1,
   "type" => "download",
   "enabled" => 1,
   "collapsed" => 0,
   "level" => 1,
   'fields' => [
     "download" => [$fileId],
   ]
 ];

}


public function uploadAsset($path) {


  $file = $this->_downloadImage($path->fullpath);


  if($file) {
    $asset = $this->_uploadNewAsset($file,$path->filename,1, 1);

    $this->normalMessage("File uploaded, returning Asset $asset->id");

    return $asset->id;

  } 

  $this->normalMessage("File not found, returning null");

  // If we don't get a file, return null
  return null;

}


}